#!/usr/bin/env python

# example usage: "python3 crawler.py"

import requests

print("[+] Started crawler...")

def request(url):
    try:
        return requests.get("http://" + url)
    except requests.exceptions.ConnectionError:
        pass

def crawl_subdomains(target_url):
    with open("subdomains-wordlist_easy.txt", "r") as wordlist_file:
        for line in wordlist_file:
            word = line.strip()
            test_url = f"{word}.{target_url}"
            response = request(test_url)

            if response:
                print("[+] Discovered subdomain --> " + test_url)

def crawl_subdirectories(target_url):
    with open("files-and-dirs-wordlist.txt", "r") as wordlist_file:
        for line in wordlist_file:
            word = line.strip()
            test_url = f"{target_url}/{word}"
            response = request(test_url)

            if response:
                print("[+] Discovered URL directory --> " + test_url)
            

target_url = "192.168.100.6/mutillidae"
crawl_subdomains(target_url)
crawl_subdirectories(target_url)
