#!/usr/bin/env python

# example usage: "python3 spider.py"

import requests
import re
import urllib.parse as urllib

print("[+] Started spider...")

target_url = "http://192.168.100.6/" # metasploitable
# target_url = "https://zsecurity.org"
target_links = set()

def extract_links_from(url):
    response = requests.get(url)
    return re.findall('(?:href=")(.*?)"', str(response.content))

def crawl(url):
    href_links = extract_links_from(url)
    for link in href_links:
        # make relative links absolute
        link = urllib.urljoin(url, link)

        # get rid of anchors
        if "#" in link:
            link = link.split("#")[0]

        # we don't care about external links and don't want duplicates
        if url in link and link not in target_links:
            target_links.add(link)
            print(link)

            # crawl any link that we found and start again
            crawl(link)

crawl(target_url)
